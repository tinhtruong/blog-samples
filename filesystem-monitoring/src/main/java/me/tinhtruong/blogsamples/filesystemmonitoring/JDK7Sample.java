package me.tinhtruong.blogsamples.filesystemmonitoring;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;


public class JDK7Sample {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		FileSystem fs = FileSystems.getDefault();
		try (final WatchService watchService = fs.newWatchService()) {
			Path path = Paths.get(System.getProperty("user.home"));
			WatchKey watchKey = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, 
					StandardWatchEventKinds.ENTRY_DELETE, 
					StandardWatchEventKinds.ENTRY_MODIFY);
			do {
				watchKey = watchService.take();
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					Kind<?> kind = event.kind();
					Path eventPath = (Path) event.context();
					System.out.println("Event kind" + ": " + kind + ": " + eventPath);
				}
			} while (watchKey.reset());
		}
	}
}
