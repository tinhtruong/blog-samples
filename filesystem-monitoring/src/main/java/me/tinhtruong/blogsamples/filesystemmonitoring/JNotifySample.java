package me.tinhtruong.blogsamples.filesystemmonitoring;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;

public class JNotifySample {
	
	public static void main(String[] args) throws JNotifyException {
		int masks = JNotify.FILE_CREATED | JNotify.FILE_MODIFIED | JNotify.FILE_RENAMED | JNotify.FILE_DELETED;
		String incomingFolderPath = "/path/to/folder";
		int watchId = JNotify.addWatch(incomingFolderPath, masks, false, new JNotifySample.FilesystemChangeListener());
		
		// clean up when you dont need the watch service any more.
		boolean result = JNotify.removeWatch(watchId);
		if (!result) {
			// wrong watchId
		}
	}
	
	static class FilesystemChangeListener implements JNotifyListener {

		@Override
		public void fileCreated(int arg0, String arg1, String arg2) {
			
		}

		@Override
		public void fileDeleted(int arg0, String arg1, String arg2) {
			
		}

		@Override
		public void fileModified(int arg0, String arg1, String arg2) {
			
		}

		@Override
		public void fileRenamed(int arg0, String arg1, String arg2, String arg3) {
			
		}
	}
}
