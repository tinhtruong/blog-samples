package me.tinhtruong.blogsamples.filesystemmonitoring;

import java.io.File;
import java.io.IOException;
import java.util.List;

import name.pachler.nio.file.ClosedWatchServiceException;
import name.pachler.nio.file.FileSystems;
import name.pachler.nio.file.Path;
import name.pachler.nio.file.Paths;
import name.pachler.nio.file.StandardWatchEventKind;
import name.pachler.nio.file.WatchEvent;
import name.pachler.nio.file.WatchKey;
import name.pachler.nio.file.WatchService;

public class JPathWatchSample {

	public static void main(String[] args) throws IOException, InterruptedException {
		WatchService watchService = FileSystems.getDefault().newWatchService();
		Path seedDataPath = Paths.get("");
		WatchKey watchKey = seedDataPath.register(watchService, StandardWatchEventKind.ENTRY_CREATE);
		Thread pathWatcher = new Thread(new JPathWatchSample.PathWatcher(watchService));
		pathWatcher.start();
		
		Thread.sleep(10000000);
		
		// clean up when you've done
		pathWatcher.stop();
		watchService.close();
	}
	static class PathWatcher implements Runnable {
		private WatchService watchService;
		
		public PathWatcher(WatchService watchService) {
			super();
			this.watchService = watchService;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public void run() {
			for (;;) {
				WatchKey signalledKey;
				try {
					signalledKey = watchService.take();
				} catch (InterruptedException ix){
			        continue;
			    } catch (ClosedWatchServiceException cwse) {
			        break;
			    }
				// get list of events from key
			    List<WatchEvent<?>> list = signalledKey.pollEvents();
			    signalledKey.reset();
			    for (WatchEvent e : list) {
			        if (e.kind() == StandardWatchEventKind.ENTRY_CREATE) {
			            Path context = (Path) e.context();
			            File createdFile = new File(context.toString());
			            
			        } else if (e.kind() == StandardWatchEventKind.OVERFLOW) {
			        	System.out.println("OVERFLOW: more changes happened than we could retreive");
			        } else {
			        	System.out.println("Catched an uninteresting event of kind " + e.kind() + ".");
			        }
			    }
			}
		}
	}
}
